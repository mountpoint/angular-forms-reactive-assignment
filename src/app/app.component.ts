import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public projectForm: FormGroup;
  private defaultProjectStatus: string = 'stable';

  ngOnInit(): void {
    this.projectForm = new FormGroup({
      'project_name': new FormControl(null, Validators.required, this.projectNameValidator),
      'email': new FormControl(null, [ Validators.required, Validators.email, this.projectEmailValidator ]),
      'project_status': new FormControl(this.defaultProjectStatus)
    });
  }

  onSubmit(): void {
    console.log('Project Name', this.projectForm.value.project_name);
    console.log('Email', this.projectForm.value.email);
    console.log('Status', this.projectForm.value.project_status);

    this.projectForm.reset({
      'project_name': '',
      'email': '',
      'project_status': this.defaultProjectStatus
    });
  }

  projectNameValidator(control: FormControl): Promise<any> | Observable<any> {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        control.value.toLowerCase() === 'test' ? resolve({ 'projectNameForbidden': true }) : resolve(null);
      }, 1000);
    });
  }

  projectEmailValidator(control: FormControl): { [s: string]: boolean } {
    return control.value === 'test@mail.com' ? { 'emailForbidden': true } : null;
  }
}
